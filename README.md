Android Assignment Movie Project
=========================

This is the application for Android Assignment Movie Projects.
First we call the API form web and also check the internet check validations.



The API url is:
https://api.themoviedb.org/4/list/1?language=en&api_key=<API_KEY>


First time this api is called and we save the response in our Local database (As I integrated Room DB)
and we save this data in our Database till 2 hours. within 2 hours if we again open app then it will not call the api and it will get data from DB.
After two hours it will recall the api and again save the data till next 2 hours.



If we want the updated data then we can pull to refresh (swipe the list from top to bottom) then new data
will be fetched from api
Here  user can search the movie using "Movie name".

By clicking any Item (Movie you will be navigate to movie detail page)



* In this project we are using the following technologies:


Language: Kotlin


Architecture: MVVM (Model View View Model)


Design Pattern: Kodein, Coroutine


API Call: Retrofit


Database Room DB


MinimumSDK version: 21

Unit Test cases Included
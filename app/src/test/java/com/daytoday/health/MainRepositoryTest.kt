package com.daytoday.health

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.daytoday.health.data.db.AppDataBase
import com.daytoday.health.data.network.MyApi
import com.daytoday.health.data.network.SafeApiRequest
import com.daytoday.health.data.network.response.MovieResponse
import com.daytoday.health.data.prefrences.PreferenceProvider
import com.daytoday.health.data.repository.MainRepository
import com.daytoday.health.data.repository.MainRepository.Companion.API_KEY
import com.daytoday.health.data.repository.MainRepository.Companion.AUTHENTICATION
import com.daytoday.health.data.repository.MainRepository.Companion.CONTENT_TYPE
import com.daytoday.health.data.repository.MainRepository.Companion.LANGUAGE
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainRepositoryTest {

    @Mock
    private lateinit var myAPi: MyApi

    @Mock
    private lateinit var safeApiRequest: SafeApiRequest

    @Mock
    private lateinit var appDataBase: AppDataBase

    @Mock
    private lateinit var preference: PreferenceProvider

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var mainRepository: MainRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        mainRepository = MainRepository(myAPi, appDataBase, preference)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }
    @Test
    fun `main repo executes api call`(){
        testScope.runBlockingTest {

            //Given
            val popularRepositoryData = Mockito.mock(MovieResponse::class.java)
            whenever( safeApiRequest.apiRequest { myAPi.getMovies(CONTENT_TYPE, AUTHENTICATION, API_KEY, LANGUAGE)}).thenReturn(popularRepositoryData)

            //When
            myAPi.getMovies(CONTENT_TYPE, AUTHENTICATION, API_KEY, LANGUAGE)

            //Then
            verify(myAPi, times(1)).getMovies(CONTENT_TYPE, AUTHENTICATION, API_KEY, LANGUAGE)
            verifyNoMoreInteractions(myAPi)
        }
    }
}


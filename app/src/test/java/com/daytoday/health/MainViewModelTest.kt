package com.daytoday.health

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.daytoday.health.data.network.response.MovieResponse
import com.daytoday.health.data.network.response.MovieResult
import com.daytoday.health.data.prefrences.PreferenceProvider
import com.daytoday.health.data.repository.MainRepository
import com.daytoday.health.ui.viewmodel.MainViewModel
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import kotlin.collections.ArrayList

class MainViewModelTest {

    // Mock the repository object and verify interactions on this mock
    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var preference: PreferenceProvider

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = MainViewModel(mainRepository, preference)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `main view model executes apiCall`() {

        testScope.runBlockingTest {

            val popularRepositoryData = Mockito.mock(MovieResponse::class.java)
            whenever(mainRepository.getMovieResult()).thenReturn(popularRepositoryData)

            verify(mainRepository, times(0)).getMovieResult()
            verifyNoMoreInteractions(mainRepository)
        }
    }

    @Test
    fun  `main view model executes DBCall`() {

        testScope.runBlockingTest {

            val popularRepositoryData = Mockito.mock(MovieResult::class.java)
            val repoList = listOf(popularRepositoryData)
            whenever(mainRepository.getReposFromDataBase()).thenReturn(ArrayList(repoList))

            verify(mainRepository, times(0)).getReposFromDataBase()
            verifyNoMoreInteractions(mainRepository)

        }
    }

    @Test
    fun  `main view model executes PreferenceData`() {

        testScope.runBlockingTest {

            val lastSavedDate: String? = null
            whenever(preference.getLastSavedAt()).thenReturn(lastSavedDate)

            verify(preference, times(0)).getLastSavedAt()
            verifyNoMoreInteractions(preference)
        }
    }
}
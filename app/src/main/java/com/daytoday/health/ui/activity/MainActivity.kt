package com.daytoday.health.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.daytoday.health.data.network.response.MovieResult
import com.daytoday.health.ui.adapter.MovieListAdapter
import com.daytoday.health.ui.modelfactory.MainViewModelFactory
import com.daytoday.health.ui.viewmodel.MainViewModel
import com.daytoday.health.ui.viewmodel.MainViewModel.Companion.MOVIE_RESULT
import com.daytoday.health.utils.Coroutines
import com.daytwoday.health.R
import com.daytwoday.health.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import org.parceler.Parcels


class MainActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MainViewModelFactory by instance()
    private lateinit var mainViewModel: MainViewModel
    private var movieList = ArrayList<MovieResult>()
    private var movieListTemp = ArrayList<MovieResult>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        mainViewModel =
            ViewModelProvider(this, factory).get(MainViewModel::class.java)

        binding.viewModel = mainViewModel

        setToolBar(false, "Movies")

        makeAPICall()

        mainViewModel.onFailure.observe(this, Observer {
            Coroutines.main {
                binding.errorModel = mainViewModel.errorModel
            }
        })


        mainViewModel.onStart.observe(this, Observer {
            refreshRepos.isRefreshing = false
            mainViewModel.shimmerVisible.set(true)
            mainViewModel.onError.set(false)
        })

        mainViewModel.onSuccess.observe(this, Observer {
            movieList = it
            mainViewModel.onError.set(false)
            initRecyclerView()
        })

        mainViewModel.clickLiveData.observe(this, Observer {
            Intent(this, MovieDetailsActivity::class.java).also { intent ->
                val bundle = Bundle()
                bundle.putParcelable(
                    MOVIE_RESULT,
                    Parcels.wrap(it)
                )
                intent.putExtras(bundle)
                startActivity(intent)
            }
        })

        refreshRepos.setOnRefreshListener {
            mainViewModel.apiCallToGetMovieList()
        }

        search.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(cs: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
                movieListTemp.clear()
                if (cs.length > 2) {
                    movieListTemp.clear()
                    for (item in movieList.indices) {
                        if (movieList[item].title.contains(cs.toString(), true)) {
                            movieListTemp.add(movieList[item])
                        }
                    }
                    (movieListRecyclerView.adapter as MovieListAdapter).movieList = movieListTemp
                    (movieListRecyclerView.adapter as MovieListAdapter).notifyDataSetChanged()
                } else if (cs.isEmpty()) {
                    (movieListRecyclerView.adapter as MovieListAdapter).movieList = movieList
                    (movieListRecyclerView.adapter as MovieListAdapter).notifyDataSetChanged()
                }
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
            }

            override fun afterTextChanged(arg0: Editable) {
            }
        })
    }

    private fun makeAPICall() {
        mainViewModel.getMovieList()
    }

    private fun initRecyclerView() {
        movieListRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = MovieListAdapter(context, movieList, mainViewModel)
        }
    }

}

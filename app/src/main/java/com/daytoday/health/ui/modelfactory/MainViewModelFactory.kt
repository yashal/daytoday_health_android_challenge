package com.daytoday.health.ui.modelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.daytoday.health.data.prefrences.PreferenceProvider
import com.daytoday.health.data.repository.MainRepository
import com.daytoday.health.ui.viewmodel.MainViewModel

class MainViewModelFactory(
    private val repository: MainRepository,
    private val prefs: PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(repository, prefs) as T
    }
}
package com.daytoday.health.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.daytoday.health.data.network.response.MovieResult
import com.daytoday.health.ui.modelfactory.MovieDetailsViewModelFactory
import com.daytoday.health.ui.viewmodel.MainViewModel.Companion.MOVIE_RESULT
import com.daytoday.health.ui.viewmodel.MovieDetailsViewModel
import com.daytoday.health.utils.changeDateFormat
import com.daytwoday.health.R
import com.daytwoday.health.databinding.ActivityMovieDetailsBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import org.parceler.Parcels


class MovieDetailsActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MovieDetailsViewModelFactory by instance()
    private lateinit var movieDetailsViewModel: MovieDetailsViewModel
    private var movieResult : MovieResult? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMovieDetailsBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_movie_details)

        movieDetailsViewModel =
            ViewModelProvider(this, factory).get(MovieDetailsViewModel::class.java)

        binding.viewModel = movieDetailsViewModel

        setToolBar(true, "Movie Details")

        if (Parcels.unwrap<MovieResult>(intent.getParcelableExtra(MOVIE_RESULT)) != null) {
            movieResult =
                Parcels.unwrap<MovieResult>(intent.getParcelableExtra(MOVIE_RESULT))
        }
        binding.data = movieResult

        binding.date = changeDateFormat("yyyy-MM-dd", "dd MMMM yyyy", movieResult?.release_date!!)
        binding.language = "English, Latin, Italiano"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}

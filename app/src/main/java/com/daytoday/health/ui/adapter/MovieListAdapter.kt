package com.daytoday.health.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.daytwoday.health.R
import com.daytoday.health.data.network.response.MovieResult
import com.daytoday.health.ui.viewmodel.MainViewModel
import com.daytwoday.health.databinding.LayoutMovieItemBinding


class MovieListAdapter(
    private val context: Context,
    var movieList: ArrayList<MovieResult>,
    private val model: MainViewModel
) : RecyclerView.Adapter<MovieListAdapter.MovieAdapterViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapterViewHolder {

        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutMovieItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.layout_movie_item, parent, false)
        return MovieAdapterViewHolder(binding.root, binding)
    }

    override fun getItemCount() = movieList.size

    override fun onBindViewHolder(holder: MovieAdapterViewHolder, position: Int) {
        val movieResult: MovieResult = movieList[position]
        holder.binding.data = movieResult
        holder.binding.viewModel = model

    }

    class MovieAdapterViewHolder(itemView: View, var binding: LayoutMovieItemBinding) :
        RecyclerView.ViewHolder(itemView){
    }
}
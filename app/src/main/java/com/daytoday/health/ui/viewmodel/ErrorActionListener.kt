package com.daytoday.health.ui.viewmodel


interface ErrorActionListener {
    abstract fun onErrorActionClicked()
}
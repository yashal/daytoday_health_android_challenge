package com.daytoday.health.ui.modelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.daytoday.health.ui.viewmodel.MovieDetailsViewModel

class MovieDetailsViewModelFactory(
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieDetailsViewModel() as T
    }
}
package com.daytoday.health.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.ViewModel
import com.daytoday.health.data.network.response.MovieResult
import com.daytoday.health.data.prefrences.PreferenceProvider
import com.daytoday.health.data.repository.MainRepository
import com.daytoday.health.utils.ApiException
import com.daytoday.health.utils.NoInternetException
import com.daytoday.health.utils.getCurrentTime
import kotlinx.coroutines.launch
import kotlin.collections.ArrayList

private const val MIN_INTERVAL = 60 * 60 * 2

class MainViewModel(
    private val repository: MainRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {

    // variable Declaration
    val onStart = MutableLiveData<Any>()
    val onSuccess = MutableLiveData<ArrayList<MovieResult>>()
    val onFailure = MutableLiveData<String>()
    val clickLiveData = MutableLiveData<MovieResult>()
    var shimmerVisible = ObservableField<Boolean>(true)
    val errorModel: ErrorModel by lazy { ErrorModel() }
    val onError = ObservableBoolean(false)
    val refreshing = ObservableBoolean(false)

    //  get movie List
    fun getMovieList(){
        onStart.postValue("")
        val lastSavedAt = prefs.getLastSavedAt()
        if (lastSavedAt == null || ifFetchNeeded(lastSavedAt.toLong())){

            //  If Saved Time  should not be null and should be less then 2 hrs then make api call
            apiCallToGetMovieList()
        } else {
            // Other wise call from Room DB
            viewModelScope.launch {
                try {
                    val response = repository.getReposFromDataBase()
                    response.let {
                        shimmerVisible.set(false)
                        val result = ArrayList(it)

                        if (result.size > 0) {
                            onSuccess.postValue(result)
                        } else {
                            onError.set(true)
                            showErrorModel(
                                ERROR_TITLE,
                                ERROR_DESCRIPTION
                            )
                            onFailure.postValue("")
                        }
                        return@launch
                    }
                } catch (ex: ApiException) {
                    shimmerVisible.set(false)
                    onError.set(true)
                    showErrorModel(
                        ERROR_TITLE,
                        ERROR_DESCRIPTION
                    )
                    onFailure.postValue("${ex.message}")
                    ex.printStackTrace()
                }
            }
        }
    }

    // API call
    fun apiCallToGetMovieList() {
        onStart.postValue("")
        viewModelScope.launch {
            try {
                val response = repository.getMovieResult()
                response.let {
                    shimmerVisible.set(false)
                    val result = it
                    onSuccess.postValue(result.results)
                    return@launch
                }
            } catch (ex: ApiException) {
                shimmerVisible.set(false)
                onError.set(true)
                showErrorModel(
                    MOVIE_ERROR_TITLE,
                    ERROR_DESCRIPTION
                )
                onFailure.postValue("${ex.message}")
                ex.printStackTrace()
            } catch (ex: NoInternetException) {
                shimmerVisible.set(false)
                onError.set(true)
                showErrorModel(
                    ERROR_TITLE,
                    MOVIE_ERROR_DESCRIPTION
                )
                onFailure.postValue("${ex.message}")
                ex.printStackTrace()
            }
        }
    }

    // Show Error Model
    private fun showErrorModel(
        title: String,
        subTitle: String
    ) {
        errorModel.apply {
            errorTitle = title
            errorSubTitle = subTitle
            buttonText = RETRY
            errorActionListener = object : ErrorActionListener {
                override fun onErrorActionClicked() {
                    apiCallToGetMovieList()
                }
            }
        }
    }

    //  Compare last saved time and current time
    private fun ifFetchNeeded(savedAt: Long): Boolean{
        val difference = getCurrentTime() - savedAt
        return difference > MIN_INTERVAL
    }

    fun cardClick(data: MovieResult){
        clickLiveData.postValue(data)
    }

    companion object {
        const val ERROR_TITLE = "Something went wrong.."
        const val ERROR_DESCRIPTION = "An alien is probably blocking your signal."
        const val RETRY = "RETRY"
        const val MOVIE_ERROR_TITLE = "Movie list issue"
        const val MOVIE_ERROR_DESCRIPTION = "No movie available"
        const val MOVIE_RESULT = "movie_result"
    }
}
package com.daytoday.health

import android.app.Application
import com.daytoday.health.data.db.AppDataBase
import com.daytoday.health.data.network.MyApi
import com.daytoday.health.data.network.NetworkConnectionInterceptor
import com.daytoday.health.data.prefrences.PreferenceProvider
import com.daytoday.health.data.repository.MainRepository
import com.daytoday.health.ui.modelfactory.MainViewModelFactory
import com.daytoday.health.ui.modelfactory.MovieDetailsViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class DayToDayApplication : Application(), KodeinAware{

    override val kodein = Kodein.lazy {
        import(androidXModule(this@DayToDayApplication))
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { AppDataBase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MainRepository(instance(), instance(), instance()) }
        bind() from provider { MainViewModelFactory(instance(), instance()) }
        bind() from provider { MovieDetailsViewModelFactory() }
    }
}
package com.daytoday.health.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.daytoday.health.data.network.response.MovieResult

@Database(
    entities = [MovieResult::class],
    version = 1
)
abstract class AppDataBase : RoomDatabase() {

    abstract fun getMovieRepoDao(): MovieRepoDao

    companion object {

        @Volatile
        private var instance: AppDataBase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDataBase::class.java,
                "MovieDatabase.db"
            ).fallbackToDestructiveMigration().build()
    }

}
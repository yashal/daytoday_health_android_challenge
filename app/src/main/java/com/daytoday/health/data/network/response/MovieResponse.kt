package com.daytoday.health.data.network.response

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

data class MovieResponse(
    val results: ArrayList<MovieResult>
)

@Parcelize
@Entity
data class MovieResult(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val poster_path: String,
    val popularity: Double,
    val vote_count: Long,
    val video: Boolean,
    val media_type: String,
    val adult: Boolean,
    val backdrop_path: String,
    val original_language: String,
    val original_title: String,
    val title: String,
    val vote_average: Double,
    val overview: String,
    val release_date: String,
    @Embedded
    val genre_ids: ArrayList<String>? = null
): Parcelable

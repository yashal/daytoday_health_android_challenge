package com.daytoday.health.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.daytoday.health.data.network.response.MovieResult

@Dao
interface MovieRepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllTrendingRepo(quote: List<MovieResult>)

    @Query("SELECT * FROM MovieResult")
    fun getTrendingRepo(): List<MovieResult>
}
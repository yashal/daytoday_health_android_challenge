package com.daytoday.health.data.repository

import androidx.lifecycle.MutableLiveData
import com.daytoday.health.data.db.AppDataBase
import com.daytoday.health.data.network.MyApi
import com.daytoday.health.data.network.SafeApiRequest
import com.daytoday.health.data.network.response.MovieResponse
import com.daytoday.health.data.network.response.MovieResult
import com.daytoday.health.data.prefrences.PreferenceProvider
import com.daytoday.health.utils.Coroutines
import com.daytoday.health.utils.getCurrentTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepository(
    private val api: MyApi,
    private val db: AppDataBase,
    private val prefs: PreferenceProvider
) : SafeApiRequest() {

    private val quotes = MutableLiveData<List<MovieResult>>()

    suspend fun getMovieResult(): MovieResponse {
        val response = apiRequest { api.getMovies(CONTENT_TYPE, AUTHENTICATION, API_KEY, LANGUAGE) }
        quotes.postValue(response.results)
        return response
    }

    init {
        quotes.observeForever {
            saveReposInDataBase(it)
        }
    }

    private fun saveReposInDataBase(quotes: List<MovieResult>){
        Coroutines.io{
            prefs.saveLastSavedAt(getCurrentTime().toString())
            db.getMovieRepoDao().saveAllTrendingRepo(quotes)
        }
    }

    suspend fun getReposFromDataBase(): List<MovieResult> {
        return withContext(Dispatchers.IO){
            db.getMovieRepoDao().getTrendingRepo()
        }
    }

    companion object {
        const val CONTENT_TYPE = "application/json;charset=utf-8"
        const val AUTHENTICATION = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3MzE0OTMwZjU1OThjNDRiZmU4NjJmZWUxZmQ4MzI1YiIsInN1YiI6IjVlNjVmOWE3NDU5YWQ2MDAxYTVhOGI4NyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.dddv6eTxb_wxTKo8JOv64K4p6liL-7_Jch9mqO7tKPg"
        const val API_KEY = "7314930f5598c44bfe862fee1fd8325b"
        const val LANGUAGE = "en"
    }
}
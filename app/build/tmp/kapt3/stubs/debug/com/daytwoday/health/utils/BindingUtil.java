package com.daytwoday.health.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0007\u00a8\u0006\u000b"}, d2 = {"Lcom/daytwoday/health/utils/BindingUtil;", "", "()V", "setImage", "", "imageView", "Landroid/widget/ImageView;", "url", "", "errorDrawable", "Landroid/graphics/drawable/Drawable;", "app_debug"})
public final class BindingUtil {
    public static final com.daytwoday.health.utils.BindingUtil INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"imageUrl", "errorDrawble"})
    public static final void setImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView, @org.jetbrains.annotations.Nullable()
    java.lang.String url, @org.jetbrains.annotations.Nullable()
    android.graphics.drawable.Drawable errorDrawable) {
    }
    
    private BindingUtil() {
        super();
    }
}
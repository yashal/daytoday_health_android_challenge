package com.daytwoday.health.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\n\u001a\u00020\tH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\r"}, d2 = {"Lcom/daytwoday/health/data/repository/MainRepository;", "Lcom/daytwoday/health/data/network/SafeApiRequest;", "api", "Lcom/daytwoday/health/data/network/MyApi;", "prefs", "Lcom/daytwoday/health/data/prefrences/PreferenceProvider;", "(Lcom/daytwoday/health/data/network/MyApi;Lcom/daytwoday/health/data/prefrences/PreferenceProvider;)V", "quotes", "Landroidx/lifecycle/MutableLiveData;", "Lcom/daytwoday/health/data/network/response/MovieResponse;", "getMovieResult", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "app_debug"})
public final class MainRepository extends com.daytwoday.health.data.network.SafeApiRequest {
    private final androidx.lifecycle.MutableLiveData<com.daytwoday.health.data.network.response.MovieResponse> quotes = null;
    private final com.daytwoday.health.data.network.MyApi api = null;
    private final com.daytwoday.health.data.prefrences.PreferenceProvider prefs = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CONTENT_TYPE = "application/json;charset=utf-8";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String AUTHENTICATION = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3MzE0OTMwZjU1OThjNDRiZmU4NjJmZWUxZmQ4MzI1YiIsInN1YiI6IjVlNjVmOWE3NDU5YWQ2MDAxYTVhOGI4NyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.dddv6eTxb_wxTKo8JOv64K4p6liL-7_Jch9mqO7tKPg";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_KEY = "7314930f5598c44bfe862fee1fd8325b";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LANGUAGE = "en";
    public static final com.daytwoday.health.data.repository.MainRepository.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMovieResult(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.daytwoday.health.data.network.response.MovieResponse> p0) {
        return null;
    }
    
    public MainRepository(@org.jetbrains.annotations.NotNull()
    com.daytwoday.health.data.network.MyApi api, @org.jetbrains.annotations.NotNull()
    com.daytwoday.health.data.prefrences.PreferenceProvider prefs) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/daytwoday/health/data/repository/MainRepository$Companion;", "", "()V", "API_KEY", "", "AUTHENTICATION", "CONTENT_TYPE", "LANGUAGE", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}
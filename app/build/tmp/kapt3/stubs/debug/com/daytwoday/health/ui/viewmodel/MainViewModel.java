package com.daytwoday.health.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 -2\u00020\u0001:\u0001-B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010(\u001a\u00020)J\u0018\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020\u00132\u0006\u0010,\u001a\u00020\u0013H\u0002R\u001b\u0010\u0007\u001a\u00020\b8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0015R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00180\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0015R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001f\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'\u00a8\u0006."}, d2 = {"Lcom/daytwoday/health/ui/viewmodel/MainViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lcom/daytwoday/health/data/repository/MainRepository;", "prefs", "Lcom/daytwoday/health/data/prefrences/PreferenceProvider;", "(Lcom/daytwoday/health/data/repository/MainRepository;Lcom/daytwoday/health/data/prefrences/PreferenceProvider;)V", "errorModel", "Lcom/daytwoday/health/ui/viewmodel/ErrorModel;", "getErrorModel", "()Lcom/daytwoday/health/ui/viewmodel/ErrorModel;", "errorModel$delegate", "Lkotlin/Lazy;", "onError", "Landroidx/databinding/ObservableBoolean;", "getOnError", "()Landroidx/databinding/ObservableBoolean;", "onFailure", "Landroidx/lifecycle/MutableLiveData;", "", "getOnFailure", "()Landroidx/lifecycle/MutableLiveData;", "onSort", "Ljava/util/ArrayList;", "Lcom/daytwoday/health/data/network/response/MovieResponse;", "getOnSort", "onStart", "", "getOnStart", "onSuccess", "getOnSuccess", "refreshing", "getRefreshing", "shimmerVisible", "Landroidx/databinding/ObservableField;", "", "getShimmerVisible", "()Landroidx/databinding/ObservableField;", "setShimmerVisible", "(Landroidx/databinding/ObservableField;)V", "apiCallToGetRepository", "", "showErrorModel", "title", "subTitle", "Companion", "app_debug"})
public final class MainViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> onStart = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.daytwoday.health.data.network.response.MovieResponse> onSuccess = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> onFailure = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.databinding.ObservableField<java.lang.Boolean> shimmerVisible;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy errorModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.databinding.ObservableBoolean onError = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.databinding.ObservableBoolean refreshing = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.daytwoday.health.data.network.response.MovieResponse>> onSort = null;
    private final com.daytwoday.health.data.repository.MainRepository repository = null;
    private final com.daytwoday.health.data.prefrences.PreferenceProvider prefs = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ERROR_TITLE = "Something went wrong..";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ERROR_DESCRIPTION = "An alien is probably blocking your signal.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RETRY = "RETRY";
    public static final com.daytwoday.health.ui.viewmodel.MainViewModel.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Object> getOnStart() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.daytwoday.health.data.network.response.MovieResponse> getOnSuccess() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getOnFailure() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.Boolean> getShimmerVisible() {
        return null;
    }
    
    public final void setShimmerVisible(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ObservableField<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.daytwoday.health.ui.viewmodel.ErrorModel getErrorModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableBoolean getOnError() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableBoolean getRefreshing() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.daytwoday.health.data.network.response.MovieResponse>> getOnSort() {
        return null;
    }
    
    public final void apiCallToGetRepository() {
    }
    
    private final void showErrorModel(java.lang.String title, java.lang.String subTitle) {
    }
    
    public MainViewModel(@org.jetbrains.annotations.NotNull()
    com.daytwoday.health.data.repository.MainRepository repository, @org.jetbrains.annotations.NotNull()
    com.daytwoday.health.data.prefrences.PreferenceProvider prefs) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/daytwoday/health/ui/viewmodel/MainViewModel$Companion;", "", "()V", "ERROR_DESCRIPTION", "", "ERROR_TITLE", "RETRY", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}
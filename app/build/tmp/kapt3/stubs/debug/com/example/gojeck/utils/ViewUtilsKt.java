package com.example.gojeck.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0006\u0010\u0000\u001a\u00020\u0001\u001a\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u00a8\u0006\u0006"}, d2 = {"getCurrentTime", "", "getSimpleDateFormat", "Ljava/text/SimpleDateFormat;", "dateFormat", "Lcom/example/gojeck/utils/DateFormat;", "app_debug"})
public final class ViewUtilsKt {
    
    @org.jetbrains.annotations.Nullable()
    public static final java.text.SimpleDateFormat getSimpleDateFormat(@org.jetbrains.annotations.NotNull()
    com.example.gojeck.utils.DateFormat dateFormat) {
        return null;
    }
    
    public static final long getCurrentTime() {
        return 0L;
    }
}
package com.daytwoday.health.databinding;
import com.daytwoday.health.R;
import com.daytwoday.health.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutMovieItemBindingImpl extends LayoutMovieItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.ImageView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutMovieItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private LayoutMovieItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.card.MaterialCardView) bindings[0]
            );
        this.materialCardView.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.daytwoday.health.ui.viewmodel.MainViewModel) variable);
        }
        else if (BR.data == variableId) {
            setData((com.daytwoday.health.data.network.response.MovieResult) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.daytwoday.health.ui.viewmodel.MainViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }
    public void setData(@Nullable com.daytwoday.health.data.network.response.MovieResult Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataTitle = null;
        java.lang.String dataVoteAverageJavaLangString = null;
        java.lang.String javaLangStringHttpsImageTmdbOrgTPW500DataPosterPath = null;
        com.daytwoday.health.data.network.response.MovieResult data = mData;
        java.lang.String dataPosterPath = null;
        double dataVoteAverage = 0.0;

        if ((dirtyFlags & 0x6L) != 0) {



                if (data != null) {
                    // read data.title
                    dataTitle = data.getTitle();
                    // read data.poster_path
                    dataPosterPath = data.getPoster_path();
                    // read data.vote_average
                    dataVoteAverage = data.getVote_average();
                }


                // read ("https://image.tmdb.org/t/p/w500/") + (data.poster_path)
                javaLangStringHttpsImageTmdbOrgTPW500DataPosterPath = ("https://image.tmdb.org/t/p/w500/") + (dataPosterPath);
                // read (data.vote_average) + ("")
                dataVoteAverageJavaLangString = (dataVoteAverage) + ("");
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.daytwoday.health.utils.BindingUtil.setImage(this.mboundView1, javaLangStringHttpsImageTmdbOrgTPW500DataPosterPath, getDrawableFromResource(mboundView1, R.drawable.ic_image_placeholder_wrapper));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, dataTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, dataVoteAverageJavaLangString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): data
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}